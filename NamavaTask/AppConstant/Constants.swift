//
//  SecretShares.swift
//  NamavaTask
//
//  Created by Ali on 2/14/23.
//

import Foundation

struct Constants{
    struct Vimo{
        static let vimo_base_url = "https://api.vimeo.com/"
        static let vimo_video_config_url = "https://player.vimeo.com/video/{id}/config"
        static let public_token = "0a3f7270906f44e5bf7df8e5b4f57172"
    }
}
