//
//  Utiles.swift
//  NamavaTaskTests
//
//  Created by Ali on 2/17/23.
//

import Foundation

class Utiles{

  @discardableResult
   static func registerMock(for url:URL,data:[Mock.HTTPMethod: Data],dataType:Mock.DataType = .json,statusCode:Int = 200)->Mock{

        let mock = Mock(url: url, dataType: .json, statusCode: statusCode, data: data)
        mock.register()
        return mock
    }
   
     static func loadFileFromLocalPath(_ localFilePath: String) ->Data? {
       return try? Data(contentsOf: URL(fileURLWithPath: localFilePath))
    }
}
