//
//  ListViewControllerSpec.swift
//  NamavaTaskTests
//
//  Created by Ali on 2/17/23.
//

import Foundation
import Quick
import Nimble
import RxSwift
import RxTest
import RxNimble
@testable import NamavaTask

class ListViewControllerSpec:QuickSpec{
    override func spec() {
        describe("ListViewModelSpec"){
            context("when request for search"){
                
                let searchRequest = SearchRequest(query: "apple")
                let requestUrl:URL = {
                    let rawURL = URL(string: Constants.Vimo.vimo_base_url + "videos")!
                    var urlComps = URLComponents(url:rawURL, resolvingAgainstBaseURL: false)!
                    urlComps.queryItems = searchRequest.asURLQueryItems
                    return urlComps.url!
                }()
                
                var apiRepository:ApiRepositoryProtocol!
                var viewModel:ListViewModel!
                var urlSession:URLSession!
                
                beforeEach {
                    let configuration = URLSessionConfiguration.default
                    configuration.protocolClasses = [MockingURLProtocol.self]
                    urlSession = URLSession(configuration: configuration)
                    
                    apiRepository = ApiRepository()
                    apiRepository.session = urlSession
                }
                
                context("if success"){
                    
                    beforeEach {
                        Mocker.removeAll()
                        Utiles.registerMock(for: requestUrl, data:[.get: MockedData.searchRawData])
                        
                        viewModel = ListViewModel()
                        viewModel.apiRepository = apiRepository
                        viewModel.search(for: searchRequest)
                    }
                    
                    
                    it("dataRelay has a value"){
                        //MARK: Testing
                        let result = try! viewModel.dataRelay.toBlocking().first()
                        
                        //MARK: Assertation
                        expect(result) == MockedData.searchData.data
                        
                    }
                    it("status become feach"){

                        //MARK: Testing
                        let result = try! viewModel.statusRelay.toBlocking().first()
                        
                        //MARK: Assertation
                        var isPass = false
                        switch result{
                        case .fetched:isPass = true
                        default:isPass = false
                        }
                        expect(isPass).to(beTrue())
                        
                    }
                }
                
                context("if falied"){
                    
                    beforeEach {
                        Mocker.removeAll()
                        Utiles.registerMock(for: requestUrl, data:[.get: Data()],statusCode: 404)
                        
                        viewModel = ListViewModel()
                        viewModel.apiRepository = apiRepository
                        viewModel.search(for: searchRequest)
                    }

                    it("status go to error state"){
                        //MARK: Testing
                        let result = try! viewModel.statusRelay.toBlocking().first()
                        
                        //MARK: Assertation
                        var isPass = false
                        switch result{
                        case .error( _):
                            isPass = true
                        default:isPass = false
                        }
                        expect(isPass).to(beTrue())
                    }
                }
            }
        }
    }
}
