//
//  MockedData.swift
//  NamavaTask
//
//  Created by Ali on 2/17/23.
//

import Foundation
@testable import NamavaTask

class MockedData{
    
    static var searchRawData:Data{
        let bundle = Bundle(for: self)
        let path = bundle.path(forResource: "SearchMockData", ofType: "json")!
        return Utiles.loadFileFromLocalPath( path )!
    }
    
    static var searchData: VimoResponse{
        try! JSONDecoder().decode(VimoResponse.self, from: searchRawData)
    }
}

